SHELL = /bin/bash

ruby_major ?= 2.6

nodejs_major ?= 12

ifeq ($(ruby_major), 2.6)
	rails_minor = 5.2
else ifeq ($(ruby_major), 2.7)
	rails_minor = 6.0
else ifeq ($(ruby_major), 3.0)
	rails_minor = 6.1
endif

build_tag ?= rails-builder-ruby-$(ruby_major)
test_tag = $(build_tag)-test
rails_test_app = gitlab-registry.oit.duke.edu/devops/containers/rails-test-app:rails-$(rails_minor)-ruby-$(ruby_major)-main

wait_for_it_commit = 81b1373f17855a4dc21156cfe1694c31d7d1792e
wait_for_it_sha256 = b7a04f38de1e51e7455ecf63151c8c7e405bd2d45a2d4e16f6419db737a125d6

.PHONY : build
build:
	DOCKER_BUILDKIT=1 docker build --pull -t $(build_tag) \
		--build-arg ruby_major="$(ruby_major)" \
		--build-arg nodejs_major="$(nodejs_major)" \
		./src

.PHONY : clean
clean:
	rm -rf ./test/test-app
	rm -f ./wait-for-it.sh

.PHONY : test
test : wait-for-it.sh test-build test-run

.PHONY : test-run
test-run:
	test_tag=$(test_tag) build_tag=$(build_tag) ./test/run

wait-for-it.sh:
	curl -sL -O https://raw.githubusercontent.com/vishnubob/wait-for-it/$(wait_for_it_commit)/wait-for-it.sh
	echo "$(wait_for_it_sha256)  wait-for-it.sh" | sha256sum -c --strict
	chmod +x wait-for-it.sh

.PHONY : test-build
test-build: test/test-app
	s2i build file://$(shell pwd)/test/test-app $(build_tag) $(test_tag) --incremental --pull-policy=never

test/test-app:
	docker pull $(rails_test_app)
	docker run --rm -d --name rails-test-app $(rails_test_app) /bin/bash -c 'sleep infinity'
	docker cp rails-test-app:/usr/src/app ./test/test-app
	docker stop rails-test-app

.PHONY : dev
dev:
	test_tag=$(test_tag) docker-compose up
