##
## Production puma config
##

#
# Threads
#
# Note: Puma >= 5 recognizes env vars:
#
# - PUMA_MIN_THREADS
# - PUMA_MAX_THREADS
# - MIN_THREADS
# - MAX_THREADS
#
threads_count = ENV.fetch("RAILS_MAX_THREADS")
threads threads_count, threads_count

workers ENV.fetch("WEB_CONCURRENCY")

preload_app!

port ENV.fetch("RAILS_PORT")

#
# For Rails >= 5.2 this is apparently no longer necessary
# per https://github.com/rails/rails/pull/31241
#
# if defined?(ActiveRecord)
#   before_fork do
#     ActiveRecord::Base.connection_pool.disconnect!
#   end
#
#   on_worker_boot do
#     ActiveRecord::Base.establish_connection
#   end
# end
#
