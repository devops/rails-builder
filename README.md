# Ruby on Rails application builder images

These images are based on the Docker official Ruby images.
`GEM_HOME` is set to `/usr/local/bundle` and Bundler is
configured to install gems to that location. (To further
ensure all gems are installed to `GEM_HOME`, `BUNDLE_USER_HOME`
is also set to `GEM_HOME`.)

Supported Ruby major versions: 2.6, 2.7, and 3.0.  Minor version
is the latest within the major version.

Rails versions have been pinned to the Ruby major version:

	Ruby 2.6: Rails 5.2
	Ruby 2.7: Rails 6.0
	Ruby 3.0: Rails 6.1

Also installed:
  - Node.js (version 14.x - others are possible)
  - npm and yarn (via Nodesource node.js package)
  - PostgreSQL client and development package

## Build

    $ make [ruby_major=[2.6|2.7|3.0]] [BUILD OPTIONS]

Build options:

	ruby_major[=2.6] -- Ruby major version of base image

	nodejs_major[=14] -- Node.js major version to install from Nodesource.

	extra_dependencies -- A space-separated list of additional OS packages to install.

## Test

	$ make clean test [ruby_major=[2.6|2.7|3.0]]

## Source-to-image

    $ s2i build [SRC] gitlab-registry.oit.duke.edu/devops/rails-builder:[RUBY_MAJOR]-[VARIANT] [OPTIONS]

Example (bash shell):

    $ s2i build "file://$(pwd)" gitlab-registry.oit.duke.edu/devops/rails-builder:2.6-main--incremental --copy

Pull and run the image for build usage, for example:

	$ docker pull gitlab-registry.oit.duke.edu/devops/rails-builder:2.6-main
    $ docker run --rm gitlab-registry.oit.duke.edu/devops/rails-builder:2.6-main

## Runtime Options

By default the assembled runtime image runs `rails server`.

Runtime variables:

    STARTUP_RAKE_TASKS - A list of rake tasks to run before starting the server (or other command)
	RAILS_ENV[=production] - Rails environment
	RAILS_LOG_LEVEL[=info] - Log level of Rails logger (STDOUT)
	RAILS_MAX_THREADS[=5] - Number of threads per worker of Rails server (puma).
	WEB_CONCURRENCY[=2] - Number of workers for the Rails server (puma).
